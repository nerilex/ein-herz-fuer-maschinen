Ein Herz für Maschinen
======================

by bg nerilex,
licensed under CC-BY-4.0


"Ein Herz für Maschinen" is German for "A heart for machines" and is inspired by the book Qualityland (by Marc-Uwe Kling).


This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
